<?php

use Tygh\Addons\Api\ApiGetProductsInfo;
use Tygh\Addons\Api\ApiPostProductsOrder;
use Tygh\Addons\ProductVariations\ServiceProvider;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
}

if ($mode == 'manage') {
    if ($_POST['start'] && $_POST['end']) {
        $api_fetch = new ApiGetProductsInfo();
        $products = $api_fetch->getProductsToImport(0);
        $count_products = $products->total;
        $count_products = $_POST['end'];
        $my_products = [];
        $new_products = 0;
        $company = '';

        for ($i = $_POST['start'] - 1; $i < $count_products; $i++) {
            $product = $api_fetch->getProductsToImport($i);
            $offers = $api_fetch->getOffers($product->items[0]->id);
            $company = $api_fetch->getCompanyObject($product->items[0]->companyId);
            $productId = $product->items[0]->id;
            $features = getFeatures($productId);
            $countParams = countParamsPackage($api_fetch, $productId);
            sortOffers($offers, false);
            removeDeletedOffers($offers);
            $offers->items = array_values($offers->items);
            $price_product = $offers->items[0]->prices;
            if (isset($price_product) && count($price_product) > 0) {
                if (count($price_product) == 1) {
                    $price_product = $offers->items[0]->prices[0]->value;
                } elseif (count($price_product) == 2) {
                    $price_product = $offers->items[0]->prices[1]->value;
                }
            }
            if ($price_product && $product->items[0]->deleted === false && !empty($product->items[0]->categories) && $countParams == 4) {
                $my_products[$i]['products']['pim_id'] = $product->items[0]->id;
                $my_products[$i]['products']['product_code'] = $product->items[0]->externalId;
                $my_products[$i]['product_descriptions']['product'] = $product->items[0]->title;
                $my_products[$i]['products_categories'] = $product->items[0]->categories;
                $my_products[$i]['product_price']['price'] = $price_product;
                $my_products[$i]['product_company']['company'] = $company;
                $my_products[$i]['features'] = $features;
            }
        }

        foreach ($my_products as $key => $my_product) {
            $isset_product = db_get_row("SELECT * FROM ?:products WHERE pim_id = ?i", $my_products[$key]['products']['pim_id']);
            if (empty($isset_product)) {

                $company_id = checkCompany($company, $my_products, $key);

                additionalInformationForImportProducts($my_products, $key, $company_id['company_id']);
                db_query('UPDATE ?:products SET barcode_params = null');
                $my_products[$key]['products']['barcode_params'] = $key;
                $product_id = db_query('INSERT INTO ?:products ?e', $my_products[$key]['products']);

                $category_name = $my_products[$key]['products_categories'][0]->category->title;
                $my_products[$key]['product_descriptions']['product_id'] = $product_id;
                $my_products[$key]['product_descriptions']['lang_code'] = 'ru';
                db_query('INSERT INTO ?:product_descriptions ?e', $my_products[$key]['product_descriptions']);

                $category_id = db_get_fields(
                    "SELECT category_id FROM ?:category_descriptions WHERE category LIKE ?l", $category_name);
                if (empty($category_id)) {
                    continue;
                }
                $data_category['product_id'] = $product_id;
                $data_category['category_id'] = $category_id[0];
                db_query('INSERT INTO ?:products_categories ?e', $data_category);

                $my_products[$key]['product_price']['product_id'] = $product_id;
                $my_products[$key]['product_price']['lower_limit'] = '1';
                db_query('INSERT INTO ?:product_prices ?e', $my_products[$key]['product_price']);

                // Запись характеристик
                foreach ($my_products[$key]['features']['attribute']['variant'] as $index => $pim_attribute) {

                    $product_feature_variant_descriptions = db_get_row("SELECT * FROM ?:product_feature_variant_descriptions WHERE variant LIKE ?l", $pim_attribute);

                    // Если значение характеристики существует
                    if (!empty($product_feature_variant_descriptions)) {

                        $product_features_values = [];
                        $product_feature_variants = db_get_row("SELECT * FROM ?:product_feature_variants WHERE variant_id = ?i", $product_feature_variant_descriptions['variant_id']);
                        $product_features_values['feature_id'] = $my_products[$key]['features']['attribute']['feature_id'][$index];
                        $product_features_values['product_id'] = $product_id;
                        $product_features_values['variant_id'] = $product_feature_variant_descriptions['variant_id'];

                    } else {

                        // Создаем ИД хр-ки
                        $product_feature_variants = [];
                        $product_feature_variants['feature_id'] = $my_products[$key]['features']['attribute']['feature_id'][$index];
                        $variant_id = db_query('INSERT INTO ?:product_feature_variants ?e', $product_feature_variants);

                        // Записываем знач. хр-ки
                        $product_feature_variant_descriptions = [];
                        $product_feature_variant_descriptions['variant_id'] = $variant_id;
                        $product_feature_variant_descriptions['variant'] = $pim_attribute;
                        $product_feature_variant_descriptions['lang_code'] = 'ru';
                        db_query('INSERT INTO ?:product_feature_variant_descriptions ?e', $product_feature_variant_descriptions);

                        // Доб. хр-ки
                        $product_features_values = [];
                        $product_features_values['feature_id'] = $product_feature_variants['feature_id'];
                        $product_features_values['product_id'] = $product_id;
                        $product_features_values['variant_id'] = $variant_id;
                    }

                    $product_features_values['lang_code'] = 'ru';
                    if ($my_products[$key]['features']['attribute']['type'][$index] == 'integer' || 'float') {
                        $product_features_values['value_int'] = $pim_attribute;
                    }

                    db_query('INSERT INTO ?:product_features_values ?e', $product_features_values);
                }

                $new_products++;
            }
        }
        fn_set_notification('N', __('notice'), __('Новых продуктов: ' . $new_products));
    }
    if ($_POST['clean_db'] == "true") {
        cleanTables();
    }
    $api_fetch = new ApiGetProductsInfo();
    $products = $api_fetch->getProductsToImport(0);
    $products_items = $products->total;
    $lastLoadProduct = db_get_fields("SELECT barcode_params FROM ?:products WHERE barcode_params != ''");
    Tygh::$app['view']->assign(array('products_items' => $products_items, 'lastLoadProduct' => $lastLoadProduct[0]));
}

function additionalInformationForImportProducts(&$my_products, $key, $company_id)
{
    $my_products[$key]['products']['company_id'] = $company_id;
    $my_products[$key]['products']['amount'] = 1;
    $my_products[$key]['products']['timestamp'] = time();
    $my_products[$key]['products']['updated_timestamp'] = time();
    $my_products[$key]['products']['tracking'] = 'B';
    $my_products[$key]['products']['zero_price_action'] = 'R';
    $my_products[$key]['products']['is_pbp'] = 'Y';
    $my_products[$key]['products']['min_qty'] = '0';
    $my_products[$key]['products']['max_qty'] = '0';
    $my_products[$key]['products']['qty_step'] = '0';
    $my_products[$key]['products']['list_qty_count'] = '0';
    $my_products[$key]['products']['options_type'] = 'P';
    $my_products[$key]['products']['exceptions_type'] = 'F';
    $my_products[$key]['products']['details_layout'] = 'default';
    $my_products[$key]['products']['shipping_params'] = "a:5:{s:16:\"min_items_in_box\";i:0;s:16:\"max_items_in_box\";i:0;s:10:\"box_length\";i:0;s:9:\"box_width\";i:0;s:10:\"box_height\";i:0;}";
}

function countParamsPackage($api_fetch, $productId): int
{
    $attributes = $api_fetch->getProductById($productId);
    $count_params = 0;
    $params_array = ['вес', 'длина упаковки', 'ширина упаковки', 'высота упаковки'];

    foreach ($attributes->attributeValues as $attribute) {
        foreach ($attribute->translations as $weight) {
            foreach ($params_array as $value) {
                if (false !== mb_stripos($weight->title, $value)) {
                    if (!empty($attribute->value)) {
                        $count_params++;
                    }
                }
            }
        }
    }

    return $count_params;
}

function getFeatures($productId): array
{
    $api_fetch = new ApiGetProductsInfo();
    $attributes = $api_fetch->getProductById($productId);
    $features = [];

    foreach ($attributes->attributeValues as $attribute) {
        if (isset($attribute->code) && $attribute->type != 'text') {
            $product_features_descriptions = db_get_row("SELECT * FROM ?:product_features_descriptions WHERE feature_id = ?i", $attribute->code);
            if (!empty($product_features_descriptions)) {
                $features['attribute']['feature_id'][] = $attribute->code;
                $features['attribute']['type'][] = $attribute->type;
                if (is_array($attribute->value)) {
                    $features['attribute']['variant'][] = $attribute->value[0];
                } else {
                    $features['attribute']['variant'][] = $attribute->value;
                }

            }
        }
    }
    return $features;
}

function cleanTables()
{
    db_query('DELETE FROM ?:products');
    db_query('DELETE FROM ?:product_descriptions');
    db_query('DELETE FROM ?:products_categories');
    db_query('DELETE FROM ?:product_prices');
    db_query('DELETE FROM ?:product_feature_variant_descriptions');
    db_query('DELETE FROM ?:product_features_values');
    db_query('DELETE FROM ?:product_feature_variants');
}

function checkCompany($company, $my_products, $key): array {
    $company_id = null;
    $company_id_ollq = db_get_row("SELECT * FROM ?:companies WHERE company LIKE ?l", 'ollq');

    if ($company != null) {
        $company_id = db_get_row(
            "SELECT * FROM ?:companies WHERE company LIKE ?l",
            stripcslashes($my_products[$key]['product_company']['company']->name)
        );
    }

    if (!$company_id) {
        $company_id = $company_id_ollq;
    }

    return $company_id;
}