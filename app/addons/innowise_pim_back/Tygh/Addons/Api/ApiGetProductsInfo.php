<?php

namespace Tygh\Addons\Api;

class ApiGetProductsInfo extends Api
{
    public function getOmsOfferId($companyId, $externalId)
    {
        $this->api_url = $this->urlArray('getOmsOfferId', $companyId, $externalId);
        $productOffer = $this->curlGet($this->api_url);
        $productOffer = json_decode($productOffer);

        return $productOffer->items[0];
    }

    public function getCompanyObject($companyId)
    {
        if ($companyId === null) {
            return null;
        }
        $this->api_url = $this->urlArray('getCompanyObject', $companyId);
        $company = $this->curlGet($this->api_url);
        $company = json_decode($company);

        return $company;
    }

    public function getProducts($externalId)
    {
        $this->api_url = $this->urlArray('getProducts', $externalId);
        $products = $this->curlGet($this->api_url);
        $products = json_decode($products);

        return $products->items;
    }

    public function getProductByPimId($id)
    {
        $this->api_url = $this->urlArray('getProductByPimId', $id);
        $product = $this->curlGet($this->api_url);
        $product = json_decode($product);

        return $product->items[0];
    }

    public function getProductImageByeEternalId($id)
    {
        $this->api_url = $this->urlArray('getProductImageByeEternalId', $id);
        $image = $this->curlGet($this->api_url);
        $image = json_decode($image);
        $images[] = $image->media[0]->origin;
//        foreach ($image->items as $item) {
//            $images[] = $item->thumb;
//        }

        return $images;
    }

    public function getProductDescriptionAndAttributes($companyId, $externalId)
    {
        $this->api_url = $this->urlArray('getProductDescriptionAndAttributes', $companyId, $externalId);
        $product = $this->curlGet($this->api_url);
        $product = json_decode($product);

        return $product;
    }

    public function getProductInfoByPimId($id)
    {
        $this->api_url = $this->urlArray('getProductInfoByPimId', $id);

        return $this->getProductInfo();
    }

    public function getProductInfoByPimExternalId($externalId, $companyId)
    {
        $this->api_url = $this->urlArray('getProductInfoByPimExternalId', $companyId, $externalId);

        return $this->getProductInfo();
    }

    public function getProductInfo()
    {
        $product = $this->curlGet($this->api_url);
        $product = json_decode($product);
        foreach ($product->items[0]->prices as $price) {
            if ($price->type == "old_price") {
                $product_data['old_price'] = $price->value;
            } elseif ($price->type == "price") {
                $product_data['price'] = $price->value;
            }
        }
        $product_data['amount'] = $product->items[0]->quantities[0]->value;
        $product_data['items'] = $product->items;

        return $product_data;
    }

    public function getProductById($id)
    {
        $this->api_url = $this->urlArray('getProductById', $id);
        $product = $this->curlGet($this->api_url);
        $product = json_decode($product);

        return $product;
    }

    public function getOffers($productId)
    {
        $this->api_url = $this->urlArray('getProductInfoByPimId', $productId);
        $offers = $this->curlGet($this->api_url);
        $offers = json_decode($offers);

        return $offers;
    }

    public function getProductsToImport($iterator)
    {
        $this->api_url = $this->urlArray('getProductsToImport', $iterator);
        $products = $this->curlGet($this->api_url);
        $products = json_decode($products);

        return $products;
    }

    public function getDeliveryMethod()
    {
        $this->api_url = $this->urlArray('getDeliveryMethod');
        $product = $this->curlGet($this->api_url);
        $product = json_decode($product);

        return $product;
    }

    public function getOrders()
    {
        $this->api_url = $this->urlArray('getOrders');
        $orders = $this->curlGet($this->api_url);
        $orders = json_decode($orders);

        return $orders;
    }

    public function getOrdersById($id)
    {
        $this->api_url = $this->urlArray('getOrdersById', $id);
        $ordersInfo = $this->curlGet($this->api_url);
        $ordersInfo = json_decode($ordersInfo);

        return $ordersInfo;
    }
}