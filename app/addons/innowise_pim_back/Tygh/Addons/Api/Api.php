<?php

namespace Tygh\Addons\Api;

class Api
{
    protected $api_url;

    private $apikey;

    public function __construct()
    {
        $this->apikey = "Bearer 2499cf64d54dbee67ceecdaa74b4a15b7b1cae2f247356442691e8d13833dfe2";
        //$this->apikey = "Bearer 31dc5eee8a8c85808d7cb05036a6874b054f142c73f4e000b8dc7a5f7b3c3f4f"; OLD key
        //$this->apikey = "Bearer 2499cf64d54dbee67ceecdaa74b4a15b7b1cae2f247356442691e8d13833dfe2"; NEW key
    }

    protected function curlGet($url){
        $this->api_url = $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: " . $this->apikey));
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    protected function curlPost($url, $product){
        $this->api_url = $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: " . $this->apikey,
            'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    $product);
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    protected function urlArray($key, $param1 = null, $param2 = null)
    {
        $urlArray = [
            'getOmsOfferId' => "https://api.platform.ollq.ru/external/v1/offers/companies/" . $param1 . "/products/" . $param2 . "/offers",
            'getCompanyObject' => "https://api.platform.ollq.ru/external/v1/mas/companies/" . $param1,
            'getProducts' => "https://api.platform.ollq.ru/external/v2/pim/products?filter[externalId]=" . $param1,
            'getProductByPimId' => "https://api.platform.ollq.ru/external/v2/pim/products?filter[ids][]=" . $param1,
            'getProductImageByeEternalId' => "https://api.platform.ollq.ru/external/v2/pim/products/" . $param1,
            'getProductDescriptionAndAttributes' => "https://api.platform.ollq.ru/external/v2/pim/companies/" . $param1 . "/products/" . $param2,
            'getProductInfoByPimId' => "https://api.platform.ollq.ru/external/v1/offers/products/" . $param1 . "/offers",
            'getProductInfoByPimExternalId' => "https://api.platform.ollq.ru/external/v1/offers/companies/". $param1 . "/products/" . $param2 . "/offers",
            'getProductById' => "https://api.platform.ollq.ru/external/v2/pim/products/" . $param1,
            'postOrder' => "https://api.platform.ollq.ru/external/v1/oms/orders",
            'getProductsToImport' => "https://api.platform.ollq.ru/external/v2/pim/products?sort[]=id&offset=". $param1 ."&limit=1",
            'getDeliveryMethod' => "https://api.platform.ollq.ru/external/v1/deliveries/delivery-methods",
            'getOrders' => "https://api.platform.ollq.ru/external/v1/oms/orders/",
            'getOrdersById' => "https://api.platform.ollq.ru/external/v1/oms/orders/" . $param1
        ];
        return $urlArray[$key];
    }
}