<?php

namespace Tygh\Addons\Api;

class ApiPostProductsOrder extends Api
{
    public function postOrder($product)
    {
        $this->api_url = $this->urlArray('postOrder');
        $postResponse = $this->curlPost($this->api_url, $product);
        $postResponse = json_decode($postResponse);
        return $postResponse;
    }
}