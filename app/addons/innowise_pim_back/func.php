<?php

use Tygh\Addons\Api\ApiGetProductsInfo;
use Tygh\Addons\Api\ApiPostProductsOrder;
use Tygh\Addons\ProductVariations\ServiceProvider;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

/* Хуки */
// Подключение к хуку отображения продуктов
function fn_innowise_pim_back_get_products_post(&$products, $params, $lang_code)
{
    if ($params['area'] == 'C' && $params['vendor_products_by_product_id']) {
        $product_repository = ServiceProvider::getProductRepository();
        $product_data = $product_repository->findProduct($_REQUEST['product_id']);

        productsDataArticle($product_data, $params, $products);
    }
    // Подтягиваем кол-во товаров для списка товаров.
    if (!$_GET['dispatch'] == 'products.manage') {
        amountListProducts($params, $products);
    }
}

// Подключение к хуку добавления товара (начало события)
function fn_innowise_pim_back_pre_add_to_cart(&$product_data, &$cart, &$auth, &$update)
{
    if (strripos(key($_REQUEST['product_data']), 'PIM')) {
        clickToAddProductPim($update, $product_data);
    } elseif (checkProductForPimId($_REQUEST['product_data'][key($_REQUEST['product_data'])]['product_id'])) {
        clickToAddProductPim($update, $product_data);
    }
}

// Подключение к хуку добавления товара (начало события)
function fn_innowise_pim_back_check_add_to_cart_post($cart, $product, $product_id, &$result)
{
    if (strripos(key($_REQUEST['product_data']), 'PIM')) {
        setCorrectResult($result);
    } elseif (checkProductForPimId($_REQUEST['product_data'][key($_REQUEST['product_data'])]['product_id'])) {
        setCorrectResult($result);
    }
}

function fn_innowise_pim_back_get_product_price_post(&$product_id, &$amount, $auth, &$price)
{
    if (strripos(key($_REQUEST['product_data']), 'PIM')) {
        setPriceFromPim($price);
    } elseif (checkProductForPimId($_REQUEST['product_data'][key($_REQUEST['product_data'])]['product_id'])) {
        if ($price == null || $price == "0.000000") {
            setPriceFromPim($price);
        }
    }
}

// Подключение к хуку формирования корзины (начало события)
function fn_innowise_pim_back_pre_get_cart_product_data(&$hash, &$product, $skip_promotion, &$cart, $auth, $promotion_amount, &$fields, &$join, $params)
{
    if (checkProductForPimId($_SESSION['cart']['products'][$hash]['product_id'])) {
        setDummyGetHashCurrentProduct($product, $hash);
    } elseif ($product['product_code'] == "") {
        setDummyGetHashCurrentProduct($product, $hash);
    }
}

// Подключение к хуку формирования корзины (средина события)
function fn_innowise_pim_back_get_cart_product_data(&$product_id, &$_pdata, &$product, $auth, $cart, &$hash)
{
    if (checkProductForPimId($product_id)) {
        $api_fetch = new ApiGetProductsInfo();
        $arrayHash = $_SESSION['myHash'];
        unset($arrayHash[count($arrayHash) - 1]);
        if (isset($_SESSION['current_product']) && $hash != $_SESSION['last_hash'] && (!in_array(
                $hash,
                $arrayHash
            )) || empty($arrayHash) && !isset($_SESSION['cart']['products'])) {
            setDataCurrentProduct($product_id, $api_fetch, $_pdata, $hash, $cart);
            $_SESSION['myHash'][] = $hash;
        } else {
            getDataProductCart($api_fetch, $_pdata, $hash, $cart);
        }
        //вывод нужных параметров для расчета стоимости доставки
        $api_fetch = new ApiGetProductsInfo();
        $product_repository = ServiceProvider::getProductRepository();
        $product_data = $product_repository->findProduct($product['product_id']);
        $attributes = $api_fetch->getProductById(
            $product_data['pim_id']
        );

        foreach ($attributes->attributeValues as $attribute) {
            foreach ($attribute->translations as $weight) {
                if (false !== mb_stripos($weight->title, 'вес')) {
                    $product_data['weight'] = $attribute->value;
                }
                if (false !== mb_stripos($weight->title, 'длина упаковки')) {
                    $length = $attribute->value;
                }
                if (false !== mb_stripos($weight->title, 'ширина упаковки')) {
                    $width = $attribute->value;
                }
                if (false !== mb_stripos($weight->title, 'высота упаковки')) {
                    $height = $attribute->value;
                }
            }
        }
        $product_data['shipping_params'] = 'a:5:{s:16:"min_items_in_box";i:1;s:16:"max_items_in_box";i:1;s:10:"box_length";i:' . $length . ';s:9:"box_width";i:' . $width . ';s:10:"box_height";i:' . $height . ';}';
    }
}

//Проверка кол-ва товаров в базе данных перед добавлением в корзину
function fn_innowise_pim_back_check_amount_in_stock_before_check($product_id, $amount, $product_options, $cart_id, $is_edp, &$original_amount, $cart, $update_id, $product, &$current_amount, $skip_error_notification)
{
    if (checkProductForPimId($product_id)) {
        $original_amount = $amount;
        $current_amount = $amount;
    }
}

// Подключение к хуку очистки корзины
function fn_innowise_pim_back_clear_cart($cart, $complete, $clear_all)
{
    if (isset($_GET['dispatch']) && $_GET['dispatch'] == 'checkout.clear') {
        clearSession();
    }
}

// Подключение к хуку создании заказа
function fn_innowise_pim_back_create_order_details($order_id, $cart, &$order_details, &$extra)
{
    if (count($cart['product_groups']) == 1) {
        $postData = preparePostOffersData($cart, $order_details);
        //Тут отправляем данные о заказаном товаре, если отправка происходит сразу после оформления
//        $api_fetch = new ApiPostProductsOrder();
//        $response = $api_fetch->postOrder($postData);
//        $orderPimId = $response->items[0]->id;
//        db_query("UPDATE ?:orders_pim SET order_pim_id = '$orderPimId' WHERE order_id = '$order_id'");

        clearSession();
        $_SESSION['hide_notification'] = true;
    }
}

function fn_innowise_pim_back_update_product_amount_pre($product_id, $amount_delta, $product_options, $sign, $tracking, &$current_amount, $product_code, $notify, $order_info, $cart_id)
{
    if (checkProductForPimId($product_id)) {
        $current_amount = $amount_delta;
    }
}

function fn_innowise_pim_back_update_product_amount(&$new_amount, $product_id, $cart_id, $tracking, $notify, $order_info, $amount_delta, $current_amount, &$original_amount, $sign)
{
    if (checkProductForPimId($product_id)) {
        $new_amount = $current_amount;
    }
}

// Подключение к хуку получение иформации о компании
function fn_innowise_pim_back_get_companies_post($params, $auth, $items_per_page, $lang_code, &$companies)
{
    // Добавить компании из сессии
    $currentCompany = 0;
    $api_fetch = new ApiGetProductsInfo();
    foreach ($_SESSION['pim_company'] as $company) {
        $companies[$_SESSION['block_pim_id'][$currentCompany]] = [];
        $companies[array_key_last($companies)]['company_id'] = $_SESSION['block_pim_id'][$currentCompany];
        $companies[array_key_last($companies)]['company'] = $company;
        $companies[array_key_last($companies)]['logos'] = array();


        $companyObj = $api_fetch->getCompanyObject($_SESSION['block_pim_id'][$currentCompany]);

        $companies[array_key_last($companies)]['location'] = $companyObj->location;
        $companies[array_key_last($companies)]['address'] = $companyObj->address;
        $companies[array_key_last($companies)]['paymentDetails']['recipientName'] = $companyObj->paymentDetails->recipientName;
        $companies[array_key_last($companies)]['paymentDetails']['govNumber'] = $companyObj->paymentDetails->govNumber;
        $companies[array_key_last($companies)]['paymentDetails']['registrationReasonCode'] = $companyObj->paymentDetails->registrationReasonCode;
        $companies[array_key_last($companies)]['paymentDetails']['bankName'] = $companyObj->paymentDetails->bankName;
        $companies[array_key_last($companies)]['paymentDetails']['bankIdentificationCode'] = $companyObj->paymentDetails->bankIdentificationCode;
        $companies[array_key_last($companies)]['paymentDetails']['bankAccountNumber'] = $companyObj->paymentDetails->bankAccountNumber;
        if ($companyObj->logoInfo == null) {
            $companies[array_key_last(
                $companies
            )]['logos']['theme']['image']['image_path'] = "https://ollq.ru/images/thumbnails/120/30/logos/2/logo_009q-am.png";
        } else {
            $companies[array_key_last(
                $companies
            )]['logos']['theme']['image']['image_path'] = $companyObj->logoInfo->url;
        }
        $currentCompany++;
    }
    unset($_SESSION['pim_company']);
}

// Тут описание товара, так же сюда можно кидать характеристики + картинку
function fn_innowise_pim_back_get_product_data_post(&$product_data, $auth, $preview, $lang_code)
{
    $api_fetch = new ApiGetProductsInfo();

    if (!$_GET['dispatch'] == 'products.update') {
        if ($product_data['pim_id']) {
            $productPim = $api_fetch->getProductById($product_data['pim_id']);

            if (!$productPim->code == 404) {
                unset($_SESSION['block_pim_id']);
                $offers = $api_fetch->getOffers($product_data['pim_id']);
                sortOffers($offers);
                removeDeletedOffers($offers);

                foreach ($offers->items[array_key_first($offers->items)]->prices as $price) {
                    if ($price->type == 'price') {
                        $newPrice = $price->value;
                    }
                }
                $product_data['amount'] = $offers->items[array_key_first($offers->items)]->quantities[0]->value;
                $product_data['price'] = $newPrice;
                $product_data['product_id'] = $offers->items[array_key_first(
                        $offers->items
                    )]->id . 'PIM' . $product_data['pim_id'];
                $_SESSION['render_product_flag'] = true;
            }
        }
        $images = $api_fetch->getProductImageByeEternalId($product_data['pim_id']);
        foreach ($productPim->media as $image) {
            $images[] = $image->origin;
            $test = array_unique($images);
        }
        if (!empty($test)) {
            if (count($test) == 1) {
                addProductMainImage($product_data, $test);
            } else {
                addProductMainImage($product_data, $test);
                addProductAdditionalImage($product_data, $test);
            }
        }
        //Тут описание
        $products = $api_fetch->getProductByPimId($product_data['pim_id']);
        if (!empty($products)) {
            $attributes = $api_fetch->getProductById(
                $product_data['pim_id']
            );
            if ($attributes->code != "404") {
                foreach ($attributes->attributeValues as $key => $attribute) {
                    if ($attribute->translations[0]->title == "Полное описание") {
                        $description = $attribute->value[0]->value;
                        unset($attributes->attributeValues[$key]);
                    }
                }
                if ($description != null) {
                    $product_data['full_description'] = $description;
                    $product_data['meta_description'] = $description;

                    $product_data['product_features'][0]['description'] = $products->attributeSetTitle;
                }
            } else {
                unset($product_data['product_features']);
            }
        }

        // Тут характеристики
        foreach ($product_data['product_features'] as $key => $value) {
            if ($key !== 0) {
                unset($product_data['product_features'][$key]);
            }
        }
        $product_data['product_features'][0]['feature_type'] = "G";
        for ($i = 0; $i < count($attributes->attributeValues); $i++) {
            if (isset($attributes->attributeValues[$i]->options[0]->translations[0]->value)) {
                $product_data['product_features'][0]['subfeatures'][$i]['variants'][$i]['variant'] = $attributes->attributeValues[$i]->options[0]->translations[0]->value;
            } else {
                if (is_array(
                        $attributes->attributeValues[$i]->value
                    ) && !empty($attributes->attributeValues[$i]->value)) {
                    foreach ($attributes->attributeValues[$i]->value as $variant) {
                        if (is_object($variant)) {
                            $product_data['product_features'][0]['subfeatures'][$i]['variants'][$i]['variant'] = strip_tags(
                                $variant->value
                            );
                        } else {
                            $product_data['product_features'][0]['subfeatures'][$i]['variants'][$i]['variant'] .= " " . $variant;
                        }
                    }
                } else {
                    $product_data['product_features'][0]['subfeatures'][$i]['variants'][$i]['variant'] = $attributes->attributeValues[$i]->value;
                }
            }
            $product_data['product_features'][0]['subfeatures'][$i]['description'] = $attributes->attributeValues[$i]->translations[0]->title;
            $product_data['product_features'][0]['subfeatures'][$i]['feature_type'] = 'S';
            $product_data['product_features'][0]['subfeatures'][$i]['variants'][$i]['selected'] = "API";
            if (empty($product_data['product_features'][0]['subfeatures'][$i]['description'])) {
                unset($product_data['product_features'][0]['subfeatures'][$i]);
            }
        }
    } elseif ($_GET['dispatch'] == 'call_requests.request') {
        $images = $api_fetch->getProductImageByeEternalId($product_data['pim_id']);
        $product_data['main_pair']['detailed']['image_path'] = $images[0];
    } else {
        //вывод нужных параметров для расчета стоимости доставки
        $attributes = $api_fetch->getProductById($product_data['pim_id']);

        foreach ($attributes->attributeValues as $attribute) {
            foreach ($attribute->translations as $param) {
                if (false !== mb_stripos($param->title, 'вес')) {
                    $weight = $attribute->value;
                }
                if (false !== mb_stripos($param->title, 'длина упаковки')) {
                    $length = $attribute->value;
                }
                if (false !== mb_stripos($param->title, 'ширина упаковки')) {
                    $width = $attribute->value;
                }
                if (false !== mb_stripos($param->title, 'высота упаковки')) {
                    $height = $attribute->value;
                }
            }
        }
        $product_data['weight'] = $weight;
        $product_data['box_length'] = $length;
        $product_data['box_width'] = $width;
        $product_data['box_height'] = $height;
        $product_data['shipping_params'] = 'a:5:{s:16:"min_items_in_box";i:1;s:16:"max_items_in_box";i:1;s:10:"box_length";i:' . $length . ';s:9:"box_width";i:' . $width . ';s:10:"box_height";i:' . $height . ';}';
    }
}

// Подключение к хуку импорта данных
function fn_innowise_pim_back_import_post($pattern, &$import_data, $options, $result, $processed_data)
{
    for ($i = 0; $i < count($import_data); $i++) {
        $updateProducts = db_query('UPDATE ?:products SET pim_id = ' . intval($import_data[$i]['ru']['formula']) . ' WHERE product_code = ' . intval($import_data[$i]['ru']['product_code']));
        unset($import_data[$i]['ru']['formula']);
    }
}

// Функция для отображения эталонных товаров.
//function fn_innowise_pim_back_get_products($params, $fields, $sortings, &$condition, $join, $sorting, $group_by, $lang_code, $having)
//{
//    $condition = str_replace("AND (products.company_id > 0 OR master_products_storefront_offers_count.count > 0)", "", $condition);
//}

// Увеличение кол-ва товара в корзине
function fn_innowise_pim_back_post_add_to_cart(&$product_data, &$cart, $auth, $update, &$ids)
{
    if (!strripos(array_key_first($product_data), "PIM") && !empty($cart['products'])) {
        foreach ($cart['products'] as $key => $product) {
            if ($product['product_id'] == $product_data[$key]['product_id'] && $product['amount'] != $product_data[$key]['amount']) {
                $currentKey = $key;
                break;
            }
        }
        $product_repository = ServiceProvider::getProductRepository();
        $test = $product_repository->findProduct($_SESSION['cart']['products'][$currentKey]['product_id']);
        $api_fetch = new ApiGetProductsInfo();
        $offers = $api_fetch->getOffers($test['pim_id']);
        sortOffers($offers);
        removeDeletedOffers($offers);
        foreach ($offers->items as $offer) {
            if ($offer->companyId == $cart['products'][$currentKey]['company_id']) {
                if ($product_data[$currentKey]['amount'] > $offer->quantities[0]->value) {
                    $cart['products'][$currentKey]['amount'] = $offer->quantities[0]->value;
                } else {
                    $cart['products'][$currentKey]['amount'] = $product_data[$currentKey]['amount'];
                }
            }
        }
    }
}

function fn_innowise_pim_back_gather_additional_product_data_params(&$product, $params)
{
    if (isset($_SESSION['render_product_flag']) == true) {
        $product['product_id'] = intval($_REQUEST['product_id']);
        unset($_SESSION['render_product_flag']);
    }
}

function fn_innowise_pim_back_gather_additional_product_data_post(&$product, $auth, $params)
{
    //вывод главной картинки в общем каталоге
    if ($_GET['dispatch'] != 'products.manage') {

        $api_fetch = new ApiGetProductsInfo();
        $productPim = $api_fetch->getProductById($product['pim_id']);
        if (empty($productPim->media)) {
            $product_repository = ServiceProvider::getProductRepository();
            $product_data = $product_repository->findProduct($product['product_id']);
            $productPim = $api_fetch->getProductById($product_data['pim_id']);
        }
        $countImages = count($productPim->media);
        $product['main_pair']['detailed']['image_path'] = $productPim->media[$countImages - 1]->origin;
    }
    foreach ($_SESSION['cart']['products'] as $key => $value) {
        if ($product['product_id'] == $_SESSION['cart']['products'][$key]['product_id'] || $product['product_code'] == $_SESSION['cart']['products'][$key]['product_code']) {
//            $product['main_pair']['detailed']['image_path'] = $_SESSION['cart']['products'][$key]['main_pair']['detailed']['image_path'];
        }
    }
}

function fn_innowise_pim_back_add_product_to_cart_get_price($product_data, $cart, $auth, $update, &$_id, $data, $product_id, $amount, $price, $zero_price_action, &$allow_add)
{
    foreach ($cart['products'] as $key => $value) {
        if ($cart['products'][$key]['product_id'] == $product_id) {
            $_id = null;
            $allow_add = false;
        }
    }
}

//вывод картинок в деталях заказа после оформления
function fn_innowise_pim_back_get_cart_product_icon($product_id, $product_data, $selected_options, &$image)
{
    if ($_GET['dispatch'] == 'orders.details') {
        if ($image === null) {
            $product_repository = ServiceProvider::getProductRepository();
            $product_data = $product_repository->findProduct($product_data['product_id']);

            $api_fetch = new ApiGetProductsInfo();
            $productPim = $api_fetch->getProductById($product_data['pim_id']);
            $countImages = count($productPim->media);
            $image['detailed']['image_path'] = $productPim->media[$countImages - 1]->origin;
        }
    }
}

//отправка заказа в пим после изменения статуса в админке
function fn_innowise_pim_back_change_order_status_post($order_id, $order_status, $status_to, $status_from, $place_order, $order_info)
{
    if ($order_status == 'G' && $status_to !=='G') {
        $isSendOrder = db_get_array("SELECT * FROM ?:orders_pim WHERE order_id = ?i", $order_id);
        if (empty($isSendOrder[0]['order_pim_id'])){
            $orderInfo = db_get_array("SELECT * FROM ?:orders_pim WHERE order_id = ?i", $order_id);
            foreach ($orderInfo as $order) {
                $postData = $order['order_pim_info'];
                $api_fetch = new ApiPostProductsOrder();
                $response = $api_fetch->postOrder($postData);
                $orderPimId = $response->items[0]->id;
                db_query("UPDATE ?:orders_pim SET order_pim_id = '$orderPimId' WHERE order_id = '$order_id'");
                db_query("UPDATE ?:orders_pim SET order_pim_status = '1' WHERE order_id = '$order_id'");

                clearSession();
                $_SESSION['hide_notification'] = true;
            }
        }
    }
}

//удаление заказа через админку
function fn_innowise_pim_back_delete_order($order_id)
{
    if ($_GET['dispatch'] == "orders.delete") {
        db_query("DELETE FROM ?:orders_pim WHERE order_id = ?i", $order_id);
    }
}

/* Функции */

function addProductMainImage(&$product_data, $images)
{
    if (isUrl($images[0])) {
        $product_data['main_pair']['detailed']['image_path'] = $images[0];
        $product_data['main_pair']['detailed']['http_image_path'] = $images[0];
        $product_data['main_pair']['detailed']['https_image_path'] = $images[0];
    } else {
        $product_data['main_pair']['detailed']['image_path'] = null;
        $product_data['main_pair']['detailed']['http_image_path'] = null;
        $product_data['main_pair']['detailed']['https_image_path'] = null;

    }
    $product_data['main_pair']['detailed']['relative_path'] = null;
}

function addProductEmptyImage(&$product_data)
{
    while (count($product_data['image_pairs']) < 3) {
        $product_data['image_pairs'][]['detailed']['image_path'] = null;
    }
}

function preparePostOffersData($cart, $order_details)
{
    $api_fetch = new ApiGetProductsInfo();
    $response = $api_fetch->getDeliveryMethod();

    if (empty($pickup_address)) {
        $pickup_address = $cart['user_data']['b_address'];
    }

    foreach ($cart['shipping'] as $data) {
        $nameShipping = $data['shipping'];

        if ($nameShipping == 'Самовывоз') {
            $pickup_address = '';
        } else {
            foreach ($data['data']['stores'] as $address) {
                $pickup_address = $address['pickup_address'];
            }
        }
    }

    $methodId = '9a085d63-0ca9-4958-bddc-21f625017d07';
    foreach ($response->items as $item) {
        if (false !== mb_stripos($item->method->name, $nameShipping)) {
            $methodId = $item->method->id;
        }
    }

    $phone = parsePhone($cart['user_data']['phone']);

    $items['items'] = [];

    $cart['user_data']['b_firstname'] = fillEmptyFields($cart['user_data']['b_firstname']);
    $cart['user_data']['b_lastname'] = fillEmptyFields($cart['user_data']['b_lastname']);
    $cart['user_data']['b_address'] = fillEmptyFields($cart['user_data']['b_address']);
    $cart['payment_info']['address'] = fillEmptyFields($cart['payment_info']['address']);

    foreach ($cart['shipping'] as $data) {
        if (false !== mb_stripos($data['shipping'], 'самовывоз')) {
            $shippingType = 'pickup';
        } else {
            $shippingType = 'todoor';
        }
    }

    if (false !== mb_stripos($cart['payment_method_data']['payment'], 'перевод')) {
        $paymentType = 'cashless';
    } else if (false !== mb_stripos($cart['payment_method_data']['payment'], 'оплата картой')) {
        $paymentType = 'bank_card';
    } else if (false !== mb_stripos($cart['payment_method_data']['payment'], 'курьеру')) {
        $paymentType = 'courier_by_card';
    } else {
        $paymentType = 'cash_on_delivery';
    }

    if (count($cart['products']) > 1) {
        foreach ($cart['products'] as $key => $value) {
            $currentProduct = $cart['products'][$key];
            $nextProduct = next($cart['products']);
            if ($currentProduct['company_id'] == $nextProduct['company_id']) {
                $newArrayProduct[] = array_merge($currentProduct);
                $newArrayProduct[] = array_merge($nextProduct);
                for ($i = 0; $i < count($newArrayProduct); $i++) {
                    $api_fetch = new ApiGetProductsInfo();
                    $product_repository = ServiceProvider::getProductRepository();
                    $product_data = $product_repository->findProduct($newArrayProduct[$i]['product_id']);
                    $offers = $api_fetch->getOffers($product_data['pim_id']);
                    foreach ($offers->items as $offer) {
                        if ($offer->companyId == $newArrayProduct[$i]['company_id']) {
                            $offerId = $offer->id;
                        }
                    }
                    foreach ($cart['shipping'] as $shipping) {
                        $costShipping = $shipping['rate'];
                    }
                    $itemsOrder[$i]['offerId'] = $offerId;
                    $itemsOrder[$i]['quantity'] = $newArrayProduct[$i]['amount'];
                    $itemsOrder[$i]['regularPrice'] = 0;
                }
                for ($i = 0; $i < count($itemsOrder); $i++) {
                    $itemsOneMerchant[] =
                        [
                            'offerId' => $itemsOrder[$i]['offerId'],
                            'quantity' => $itemsOrder[$i]['quantity'],
                            'regularPrice' => $itemsOrder[$i]['regularPrice'],
                        ];
                }
            }
        }
        $items['items'][] =
            [
                'companyId' => $value['company_id'],
                'externalId' => $value['product_code'],
                'items' => $itemsOneMerchant,
                'address' => [
                    'firstName' => $cart['user_data']['b_firstname'],
                    'lastName' => $cart['user_data']['b_lastname'],
                    'email' => $cart['user_data']['email'],
                    'phone' => $phone,
                    'timezone' => "Europe/Kiev",
                    'delivery' => [
                        'methodId' => $methodId,
                        'type' => $shippingType,
                        'address' => $pickup_address,
                        'cost' => $costShipping,
                        'costIsPayed' => true,
                    ]
                ],
                'comment' => $cart['notes'],
                'payed' => true,
                'paymentMethod' => [
                    'code' => $paymentType,
                ],
                'currencyRates' => [
                    [
                        'code' => 'RUB',
                        'rate' => $cart['total'],
                    ]
                ]
            ];
    } else {
        foreach ($cart['products'] as $key => $item) {
            if ($key == $order_details['item_id']) {
                $api_fetch = new ApiGetProductsInfo();
                $product_repository = ServiceProvider::getProductRepository();
                $product_data = $product_repository->findProduct($item['product_id']);
                $offers = $api_fetch->getOffers($product_data['pim_id']);
                foreach ($offers->items as $offer) {
                    if ($offer->companyId == $item['company_id']) {
                        $offerId = $offer->id;
                    }
                }
                foreach ($cart['shipping'] as $shipping) {
                    $costShipping = $shipping['rate'];
                }
                $items['items'][] =
                    [
                        'companyId' => $item['company_id'],
                        'externalId' => $item['product_code'],
                        'items' => [
                            [
                                'offerId' => $offerId,
                                'quantity' => $item['amount'],
                                'regularPrice' => 0,
                            ]
                        ],
                        'address' => [
                            'firstName' => $cart['user_data']['b_firstname'],
                            'lastName' => $cart['user_data']['b_lastname'],
                            'email' => $cart['user_data']['email'],
                            'phone' => $phone,
                            'timezone' => "Europe/Kiev",
                            'delivery' => [
                                'methodId' => $methodId,
                                'type' => $shippingType,
                                'address' => $pickup_address,
                                'cost' => $costShipping,
                                'costIsPayed' => true,
                            ]
                        ],
                        'comment' => $cart['notes'],
                        'payed' => true,
                        'paymentMethod' => [
                            'code' => $paymentType,
                        ],
                        'currencyRates' => [
                            [
                                'code' => 'RUB',
                                'rate' => $item['price'] + $costShipping,
                            ]
                        ]
                    ];
            }

        }
    }
    // Вынести, когда функция будет переработана.
    if ($items['items'][0]['address']['firstName'] == null) {
        for ($i = 0; $i < count($items['items']); $i++) {
            $items['items'][$i]['address']['firstName'] = $cart['user_data']['b_firstname'];
            $items['items'][$i]['address']['lastName'] = $cart['user_data']['b_firstname'];
        }
    }
    $items = json_encode($items);

    $ordersPim['order_pim_info'] = $items;
    $ordersPim['order_id'] = $order_details['order_id'];
    $orderId = $order_details['order_id'];

    if(empty(db_get_row("SELECT * FROM ?:orders_pim WHERE order_id = '$orderId'"))) {
        db_query('INSERT INTO ?:orders_pim ?e', $ordersPim);
    };

    return $items;
}

function parsePhone($phone)
{
    if ($phone == '') {
        $phone = '77777777777';
    }
    $phone = str_replace("-", "", $phone);
    $phone = str_replace("(", "", $phone);
    $phone = str_replace(")", "", $phone);
    $phone = str_replace(" ", "", $phone);
    $phone = str_replace("+", "", $phone);
    return $phone;
}

function clearSession()
{
    unset($_SESSION['pim_price']);
    unset($_SESSION['pim_products']);
    unset($_SESSION['pim_dummy']);
    unset($_SESSION['save_product']);
    unset($_SESSION['fake_hash_arr']);
    unset($_SESSION['temp']);
    unset($_SESSION['current_product']);
    unset($_SESSION['myHash']);
}

function isUrl($url)
{
    return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
}

function amountListProducts($params, &$products)
{
    if (($params['area'] == 'C' && $params['items_per_page'] >= 12) || !empty($params['extend'])) {
        $api_fetch = new ApiGetProductsInfo();
        $count_product = 0;
        $price_product = 0;
        foreach ($products as $product) {
            if ($product['product_code'] != "") {
                $data_product = $api_fetch->getOffers($product['pim_id']);
                //добавить else, код ошибки 200
                if (!$data_product->code == 404) {
                    sortOffers($data_product, false);
                    foreach ($data_product->items as $productsPim) {
                        if ($productsPim->deleted == false) {
                            $count_product = $productsPim->quantities[0]->value;

                            foreach ($productsPim->prices as $price) {
                                if ($price->type == 'price') {
                                    $newPrice = $price->value;
                                }
                            }
                            $price_product = $newPrice;
                            break;
                        }
                    }
                    $products[$product['product_id']]['amount'] = $count_product;
                    $products[$product['product_id']]['price'] = $price_product;
                    $count_product = 0;
                    $price_product = 0;
                }
            }
        }
    }
}

function productsDataArticle($product_data, $params, &$products)
{
    if (isset($product_data['product_code']) && $product_data['product_code'] != '') {
        // Получение АПИ продукта
        $api_fetch = new ApiGetProductsInfo();
        $productPim = $api_fetch->getProductById($product_data['pim_id']);
        // Массив не пустой
        if (!$productPim->code == 404) {
            unset($_SESSION['block_pim_id']);
            $offers = $api_fetch->getOffers($product_data['pim_id']);
            sortOffers($offers, true);
            foreach ($offers->items as $offer) {
                if ($offer->deleted === false) {
                    $products[] = [];

                    $products[array_key_last($products)]['product_id'] = $offer->id . 'PIM' . $product_data['pim_id'];
                    $products[array_key_last($products)]['company_id'] = $offer->companyId;
                    $companyObj = $api_fetch->getCompanyObject($offer->companyId);
                    $products[array_key_last($products)]['company_name'] = $companyObj->name;

                    foreach ($offer->prices as $price) {
                        if ($price->type == 'price') {
                            $newPrice = $price->value;
                        }
                    }
                    $products[array_key_last($products)]['price'] = $newPrice;
                    $products[array_key_last($products)]['amount'] = $offer->quantities[0]->value;
                    $products[array_key_last($products)]['product_code'] = $product_data['product_code'];
                    $_SESSION['pim_company'][] = $products[array_key_last($products)]['company_name'];
                    $_SESSION['block_pim_id'][] = $offer->companyId;
                    $_SESSION['product_id'] = $product_data['pim_id'];
                }
            }
            $_SESSION['pim_dummy'] = $params['vendor_products_by_product_id'];
        }
    }
}

/* Функции добавления в корзину */

function createCurrentProductSession(&$product_data)
{
    $key = key($product_data);
    $offer_id = substr($key, 0, strrpos($key, 'PIM'));
    $product_id = substr($key, strrpos($key, 'PIM') + 3);
    $api_fetch = new ApiGetProductsInfo();

    $product_data[key($product_data)]['product_id'] = microtime(true);

    $productPim = $api_fetch->getProductById($product_id);
    $offers = $api_fetch->getOffers($product_id);
    foreach ($offers->items as $offer) {
        if ($offer->id == $offer_id) {
            $_SESSION['current_product']['id'] = $offer->id;
            $_SESSION['current_product']['company_id'] = $offer->companyId;
            foreach ($offer->prices as $price) {
                if ($price->type == 'price') {
                    $newPrice = $price->value;
                }
            }
            $_SESSION['current_product']['price'] = $newPrice;
            $_SESSION['current_product']['product_code'] = $offer->productId;
            $_SESSION['current_product']['next'] = true;
            $_SESSION['current_product']['externalId'] = $productPim->externalId;

            if ($_SESSION['current_product']['amount'] == 0) {
                foreach ($_SESSION['cart']['products'] as $key => $value) {
                    foreach ($_SESSION['cart']['products'] as $item) {
                        if ($item['product_id'] == $_SESSION['cart']['products'][$key]['product_id'] &&
                            $_SESSION['current_product']['externalId'] == $_SESSION['cart']['products'][$key]['product_code'] &&
                            $item['company_id'] == $_SESSION['current_product']['company_id']) {
                            $_SESSION['current_product']['amount'] = $_SESSION['cart']['products'][$key]['amount'];
                        }
                    }
                }
            }

            $customAmount = checkAddingProducts($product_data[key($product_data)]['amount'], $offer);
        }
    }
    /* СЮДА !!!!! */
    // ID Поменялось --> Поменять условие в IF
    foreach ($_SESSION['cart']['products'] as &$item) {
        if ($_SESSION['current_product']['externalId'] == $item['product_code'] && $item['company_id'] == $_SESSION['current_product']['company_id']) {
            $_SESSION['current_product']['next'] = false;
            $item['amount'] = $item['amount'] + $customAmount;
        }
    }
}

function setCorrectResult(&$result)
{
    if ($_SESSION['current_product']['next'] == false) {
        $result = false;
        //Записать в сессию в эмаунт
    } else {
        $result = true;
    }
}

function setPriceFromPim(&$price)
{
    if (isset($_SESSION['current_product'])) {
        $price = $_SESSION['current_product']['price'];
    }
}

function setDummyGetHashCurrentProduct(&$product, $hash)
{
    if ($_SESSION['cart']['products'][$hash]['product_code'] == "") {
        $product['product_id'] = $_SESSION['pim_dummy'];
        if (isset($_SESSION['current_product'])) {
            $_SESSION['current_product']['hash'] = $hash;
        }
    }
}

function setDataCurrentProduct(&$product_id, $api_fetch, &$_pdata, $hash, &$cart)
{
    $_pdata['product_id'] = $_SESSION['current_product']['id'];
    $cart['products'][$hash]['product_code'] = $_pdata['product_code'];
    $product['amount'] = $_SESSION['current_product']['amount'];
    $cart['products'][$hash]['amount'] = $_SESSION['current_product']['amount'];
    $companyObj = $api_fetch->getCompanyObject($_SESSION['current_product']['company_id']);
    $_pdata['company_name'] = $companyObj->name;
    $_pdata['in_stock'] = $_SESSION['current_product']['amount'];
    $_pdata['price'] = $_SESSION['current_product']['price'];
    $_SESSION['cart']['products'][$hash]['company_id'] = $_SESSION['current_product']['company_id'];
    $_SESSION['last_hash'] = $_SESSION['current_product']['hash'];
    $productPIM = $api_fetch->getProductById($_SESSION['current_product']['product_code']);
    $image = $productPIM->media[0]->origin;
    $cart['products'][$hash]['main_pair']['detailed']['image_path'] = $image;
    $cart['products'][$hash]['main_pair']['detailed']['http_image_path'] = $image;
    $cart['products'][$hash]['main_pair']['detailed']['https_image_path'] = $image;
    $cart['products'][$hash]['main_pair']['detailed']['image_x'] = '1';
    $cart['products'][$hash]['main_pair']['detailed']['image_y'] = '1';
    unset($_SESSION['current_product']);
}

//информация о товаре в корзине
function getDataProductCart($api_fetch, &$_pdata, $hash, $cart)
{
    $_pdata['product_id'] = $cart['products'][$hash]['product_id'];
    $companyObj = $api_fetch->getCompanyObject($cart['products'][$hash]['company_id']);
    $_pdata['company_name'] = $companyObj->name;
    $_pdata['amount'] = $cart['products'][$hash]['amount'];
    $_pdata['price'] = $cart['products'][$hash]['price'];
}

/* Конец */

function getAdditionalProductData($product, $api)
{
    if ($product->companyId) {
        $additionalProductData = $api->getProductInfoByPimExternalId(
            $product->externalId,
            $product->companyId
        );
    } else {
        $additionalProductData = $api->getProductInfoByPimId($product->id);
    }
    return $additionalProductData;
}

function checkProductForPimId($product_id)
{
    $product_repository = ServiceProvider::getProductRepository();
    $product_data = $product_repository->findProduct($product_id);
    if ($product_data['pim_id'] != '' || $product_data['pim_id'] != null) {
        return true;
    } else {
        return false;
    }
}

function deleteBadProducts(&$products)
{
    foreach ($products as $key => $product) {
        if ($product['price'] === null || $product['amount'] === null || $product['company_id'] === null || $product['company_name'] === null) {
            unset($products[$key]);
        }
    }
}

function sortOffers(&$offers, bool $reverse = false)
{
    for ($i = 0; $i < count($offers->items); $i++) {
        for ($j = 0; $j < count($offers->items) - 1; $j++) {
            if (!$reverse) {
                if ($offers->items[$j]->prices[1]->value > $offers->items[$j + 1]->prices[1]->value) {
                    swap($offers, $j);
                }
            } else {
                if ($offers->items[$j]->prices[1]->value < $offers->items[$j + 1]->prices[1]->value) {
                    swap($offers, $j);
                }
            }
        }
    }
}

function swap(&$offers, $j)
{
    $tmp = $offers->items[$j];
    $offers->items[$j] = $offers->items[$j + 1];
    $offers->items[$j + 1] = $tmp;
}

function removeDeletedOffers(&$offers)
{
    for ($i = 0; $i < $offers->total; $i++) {
        if ($offers->items[$i]->deleted == true) {
            unset($offers->items[$i]);
        }
    }
}

function checkAddingProducts($currentAmount, $offer)
{
    $totalAmount = $offer->quantities[0]->value;
    if (empty($_SESSION['cart']['products']) || $_SESSION['current_product']['amount'] === 0) {
        if ($currentAmount <= $totalAmount) {
            $_SESSION['current_product']['amount'] = $currentAmount;
        } else {
            $_SESSION['current_product']['amount'] = $totalAmount;
        }
    } elseif (!empty($_SESSION['cart']['products'])) {
        foreach ($_SESSION['cart']['products'] as &$item) {
            if ($_SESSION['current_product']['externalId'] == $item['product_code'] && $offer->companyId == $item['company_id']) {
                $cartAmount = $item['amount'];
                if ($cartAmount + $currentAmount <= $totalAmount) {
                    $_SESSION['current_product']['amount'] = $currentAmount;
                } else {
                    $_SESSION['current_product']['amount'] = $totalAmount - $cartAmount;
                }
            }
        }
        if ($_SESSION['current_product']['amount'] === null) {
            if ($currentAmount <= $totalAmount) {
                $_SESSION['current_product']['amount'] = $currentAmount;
            } else {
                $_SESSION['current_product']['amount'] = $totalAmount;
            }
        }
    }
    return $_SESSION['current_product']['amount'];
}

function clickToAddProductPim($update, &$product_data)
{
    if ($update == false) {
        if (strripos(key($_REQUEST['product_data']), 'PIM')) {
            createCurrentProductSession($product_data);
        } else {
            $_SESSION['pim_dummy'] = key($product_data);
            $product_repository = ServiceProvider::getProductRepository();
            $product_info = $product_repository->findProduct(key($product_data));
            $_SESSION['product_id'] = $product_info['pim_id'];
            $api_fetch = new ApiGetProductsInfo();
            $data_product = $api_fetch->getOffers($product_info['pim_id']);
            sortOffers($data_product);
            if (!empty($data_product)) {
                foreach ($data_product->items as $productsPim) {
                    if ($productsPim->deleted == false) {
                        $product_data_id = $productsPim->id . 'PIM' . $productsPim->productId;
                        unset($product_data[key($product_data)]);
                        $product_data[$product_data_id]['product_id'] = $product_data_id;
                        $product_data[$product_data_id]['amount'] = $_REQUEST['product_data'][key(
                            $_REQUEST['product_data']
                        )]['amount'];
                        break;
                    }
                }
            }
            createCurrentProductSession($product_data);
        }
    }
}

function addProductAdditionalImage(&$product_data, $images)
{
    for ($i = 0; $i < count($images); $i++) {
        if (isUrl($images[$i])) {
            $product_data['image_pairs'][$i]['detailed']['image_path'] = $images[$i];
            $product_data['image_pairs'][$i]['detailed']['http_image_path'] = $images[$i];
            $product_data['image_pairs'][$i]['detailed']['https_image_path'] = $images[$i];

        }
    }
}

function fillEmptyFields($field)
{
    if ($field == '') {
        $field = 'undefined';
    }

    return $field;
}
