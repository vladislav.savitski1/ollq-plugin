<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'get_products_post',
    'pre_add_to_cart',
    'check_add_to_cart_post',
    'get_product_price_post',
    'pre_get_cart_product_data',
    'get_cart_product_data',
    'check_amount_in_stock_before_check',
    'clear_cart',
    'create_order_details',
    'update_product_amount_pre',
    'update_product_amount',
    'get_companies_post',
    'get_product_data_post',
    'import_post',
//    'get_products',
    'post_add_to_cart',
    'gather_additional_product_data_params',
    'gather_additional_product_data_post',
    'add_product_to_cart_get_price',
    'get_cart_product_icon',
    'change_order_status_post',
    'delete_order'
);