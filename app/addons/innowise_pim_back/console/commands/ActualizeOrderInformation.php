<?php


use Tygh\Registry;

class ActualizeOrderInformation
{
    public function getExistsPimOrders() {
        return db_get_array("SELECT * FROM ?:orders_pim WHERE order_pim_id  IS NOT NULL");
    }

    public function exchangeStatuses($orderPimStatus) {
        switch($orderPimStatus) {
            case "Принят продавцом":
                $orderPimStatus = "Обработан";
                break;
            case "Подтвержден покупателем":
                $orderPimStatus = "Подтвержден";
                break;
            case "Отправлен":
                $orderPimStatus = "Передан СД Заказ";
                break;
            case "Возврат инициирован":
                $orderPimStatus = "Возврат";
                break;
        }

        return db_get_field("SELECT * FROM ?:status_descriptions WHERE description = '$orderPimStatus'");;
    }

    public function updateOrderStatus($statusLetter, $orderId) {
        return db_query("UPDATE ?:orders SET status = '$statusLetter' WHERE order_id = ?i", $orderId);
    }

    public function getAllStatuses($getStatusId) {
        return db_get_row("SELECT * FROM ?:statuses WHERE status_id = '$getStatusId'");
    }

    function fn_innowise_pim_back_change_order_status($status_to, $order_info, $place_order){

        if(@$place_order != "Y"){

            /*
                Для новых заказов у нас есть другой webhook
            */

            $order_info['new_status'] = $status_to;
            $webhookHandler = curl_init();
            $webhookHeaders  = [
                'X-Integration-Key: '.Registry::get('addons.ibice_bx24.integration_key'),
                'Content-Type: text/plain'
            ];
            curl_setopt($webhookHandler, CURLOPT_URL,				'https://cscart-bx24.widgets.ibice.ru/webhook/cscart/status_change/'.$order_info['order_id']);
            curl_setopt($webhookHandler, CURLOPT_POST, 				1);
            curl_setopt($webhookHandler, CURLOPT_RETURNTRANSFER, 	1);
            curl_setopt($webhookHandler, CURLOPT_POSTFIELDS, 		json_encode($order_info));
            curl_setopt($webhookHandler, CURLOPT_HTTPHEADER, 		$webhookHeaders);
            $webhookResult     = curl_exec($webhookHandler);
            $webhookStatusCode = curl_getinfo($webhookHandler, CURLINFO_HTTP_CODE);
            if($webhookStatusCode != 200){
                /*
                    Log unsuccessfull wehbook send try
                */
            }

        }

    }
}