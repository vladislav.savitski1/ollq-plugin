<div class="control-group">
    <label class="control-label" for="elm_pim_id">{__("pim_id")}:</label>
    <div class="controls">
        <input type="text" name="product_data[pim_id]" id="elm_pim_id" size="20"
               maxlength=32  value="{$product_data.pim_id}" class="input-large" />
    </div>
</div>