{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="pim_import_form" enctype="multipart/form-data">
    {if $products_items}
        <label class="control-label">Продуктов в ПИМ: {$products_items}</label>
        <label class="control-label">Последний загруженный продукт: {$lastLoadProduct}</label>
    {else}
        <label  class="control-label">Продукты в ПИМ не обнаружены</label>
    {/if}
    <label for="elm_category_name" class="control-label">От:</label>
    <div class="controls">
        <input type="text" name="start" id="" size="55" value="" class="input-large user-success">
    </div>
    <label for="elm_category_name" class="control-label">До:</label>
    <div class="controls">
        <input type="text" name="end" id="" size="55" value="" class="input-large user-success">
    </div>
</form>
    <div class="buttons-container">
        {include file="buttons/save_cancel.tpl" but_target_form="pim_import_form" but_name="dispatch[pim_import.manage]" cancel_action="close"}
    </div>

<form action="{"pim_import.manage"|fn_url}" method="post" name="clean_db" enctype="multipart/form-data">
    <input type="hidden" name="clean_db" value="true">
    <button type="submit">Очистить БД</button>
</form>

{/capture}
{capture name="adv_buttons"}
    {hook name="innowise_pim_back:adv_buttons"}
        {include file="common/tools.tpl" tool_href="pim_import.manage" prefix="top" hide_tools="true" title=__("start import")}
    {/hook}
{/capture}

{include file="common/mainbox.tpl" title=$page_title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=$select_languages sidebar=$smarty.capture.sidebar}
