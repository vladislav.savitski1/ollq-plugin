<?php

require_once 'app/addons/innowise_pim_back/console/commands/ActualizeOrderInformation.php';

define('AREA', 'C');

use Tygh\Addons\Api\ApiGetProductsInfo;

define('NO_SESSION', true);
@ini_set('display_errors', 'On');
require(dirname(__FILE__) . '/init.php');

$exit_code = 0;
try {
    $actualizeOrderInformation = new ActualizeOrderInformation();
    $issetOrder = $actualizeOrderInformation->getExistsPimOrders();

    foreach ($issetOrder as $order) {
        $api_fetch = new ApiGetProductsInfo();
        $orderList = $api_fetch->getOrdersById($order['order_pim_id']);
        $orderPimStatus = $orderList->status->title;
        if($orderPimStatus !==  "Новый") {
            $getStatusId = $actualizeOrderInformation->exchangeStatuses($orderPimStatus);
            $getStatus = $actualizeOrderInformation->getAllStatuses($getStatusId);
            $actualizeOrderInformation->updateOrderStatus($getStatus['status'], $order['order_id']);
            $order_infoDB = db_get_array("SELECT * FROM ?:orders WHERE order_id = ?i", $order['order_id']);
            $order_info = $order_infoDB[0];
            $place_order = false;
            $actualizeOrderInformation->fn_innowise_pim_back_change_order_status($getStatus['status'], $order_info, $place_order);
        }
    }
} catch (Exception $e) {
    $exit_code = 1;
}

if ($exit_code > 0) {
    http_response_code(500);
}
exit ($exit_code);


